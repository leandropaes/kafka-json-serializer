package br.com.leandropaes.kafkajsondeserializer.consumer;

import br.com.leandropaes.kafkajsondeserializer.custom.ContaCustomListener;
import br.com.leandropaes.kafkajsondeserializer.custom.JsonKafkaListener;
import br.com.leandropaes.kafkajsondeserializer.model.Conta;
import br.com.leandropaes.kafkajsondeserializer.model.OrdemCobranca;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.adapter.ConsumerRecordMetadata;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

@Slf4j
@Component
public class TestListener {

//    @KafkaListener(topicPartitions = {@TopicPartition(topic = "conta-topic", partitions = "0-1")}, groupId = "group1", containerFactory = "jsonKafkaListenerContainerFactory")
//    @KafkaListener(topics = "conta-topic", groupId = "group1", containerFactory = "jsonKafkaListenerContainerFactory")
    @ContaCustomListener(groupId = "group1")
    public void listen(Conta conta, ConsumerRecordMetadata metadata) {
        log.info("Timestamp:{} Offset: {} - Conta: {}",
                LocalDateTime.ofInstant(Instant.ofEpochMilli(metadata.timestamp()), TimeZone.getDefault().toZoneId()),
                metadata.offset(),
                conta);
    }

    @JsonKafkaListener(topics = "ordem-cobranca-topic", id = "group1")
//    @KafkaListener(topics = "ordem-cobranca-topic", groupId = "group1", containerFactory = "jsonKafkaListenerContainerFactory")
    public void listen(OrdemCobranca ordemCobranca, ConsumerRecordMetadata metadata) {
        log.info("Timestamp:{} Offset: {} - Ordem Cobranca: {}",
                LocalDateTime.ofInstant(Instant.ofEpochMilli(metadata.timestamp()), TimeZone.getDefault().toZoneId()),
                metadata.offset(),
                ordemCobranca);
    }
}
