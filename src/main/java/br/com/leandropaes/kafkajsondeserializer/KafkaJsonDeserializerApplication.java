package br.com.leandropaes.kafkajsondeserializer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaJsonDeserializerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaJsonDeserializerApplication.class, args);
	}

}
