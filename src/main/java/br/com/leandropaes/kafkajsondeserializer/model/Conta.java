package br.com.leandropaes.kafkajsondeserializer.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Conta implements Serializable {

    private String nome;
    private Integer canal;
}
