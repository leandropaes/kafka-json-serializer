package br.com.leandropaes.kafkajsondeserializer.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrdemCobranca implements Serializable {

    private String correlationId;
    private Double valor;
}
