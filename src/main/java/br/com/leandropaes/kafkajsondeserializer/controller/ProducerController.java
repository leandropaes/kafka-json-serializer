package br.com.leandropaes.kafkajsondeserializer.controller;

import br.com.leandropaes.kafkajsondeserializer.model.Conta;
import br.com.leandropaes.kafkajsondeserializer.model.OrdemCobranca;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.Random;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("send")
public class ProducerController {

    private final KafkaTemplate<String, Serializable> jsonKafkaTemplate;

    @GetMapping("/conta")
    public ResponseEntity<?> sendConta() {
        jsonKafkaTemplate.send("conta-topic", new Conta("Leandro", new Random().nextInt(50)));
        return ResponseEntity.ok().build();
    }

    @GetMapping("/ordem-cobranca")
    public ResponseEntity<?> sendOrdemCobranca() {
        String correlationId = UUID.randomUUID().toString();
        jsonKafkaTemplate.send("ordem-cobranca-topic", new OrdemCobranca(correlationId, new Random().nextDouble()));
        return ResponseEntity.ok().build();
    }
}
